
# Création de stories pour les composants Front Storybook (FSB)

L'objectif de la création de stories pour les composants FSB est de documenter de manière à la fois visuelle et interactive chaque composant. Cela permet aux UX de réaliser des recettes pixel-perfect et fonctionnelles sur chaque composant. Cette documentation explique comment créer des stories pour les composants FSB en utilisant Storybook 7, Vue 3, Typescript 4.9 et storybook/vite-builder pour générer des stories.

## Processus de création de stories

### Étape 1 : Création du fichier de story

Il est recommandé de créer une story pour un composant immédiatement après sa création en Vue. Créez un fichier portant le nom du composant en Pascal Case suivi de l'extension `.stories.tsx`. Dans ce fichier, vous allez placer toute la logique de la story.

**Exemple :**

```typescript

// Exemple de fichier de story pour un composant FieldLabel

// src/components/FieldLabel/FieldLabel.stories.tsx

```

### Étape 2 : Import des fonctions helpers

Importez les fonctions helpers nécessaires pour créer la story. Vous utiliserez `_renderWithWrapper` pour rendre le composant de manière uniforme. De plus, `_renderTemplate` vous permettra de rendre le code template du composant. Ces fonctions sont nécessaires en raison de problèmes avec les decorators de Storybook 7.0 pour Vue 3.

**Exemple :**

```typescript

// Import des fonctions helpers

// @ts-ignore

import { _renderWithWrapper, _renderTemplate } from '@/helpers/storybook';

```

### Étape 3 : Import des éléments de Storybook

Importez les éléments de Storybook qui vous permettront de créer le meta et les variants d'une story. Assurez-vous également d'importer des badges personnalisés de FSB pour ajouter des informations telles que la version du composant et l'état du composant.

**Exemple :**

```typescript

// Import des éléments de Storybook

import type { Meta, StoryObj } from '@storybook/vue3';

import { betaBadges } from '@sb/configs/badges';

```

### Étape 4 : Préparation de la structure de la story

Configurez la story en utilisant le meta. Le `meta` contient des informations telles que le titre, le composant, les tags, les argTypes, les valeurs par défaut (args), et les paramètres (badges) pour la story. Cela garantit une documentation claire de chaque composant.

**Exemple :**

```typescript

// Configuration de la structure de la story

const meta = {

    title: 'Path/To/MonComposant',

    component: MonComposant,

    tags: ['autodocs'],

    argTypes: {

        // Props should follow the same structure as the component's props

    },

    args: {

        // Default values

    },

    parameters: {

        badges: betaBadges,

    },

} satisfies Meta<typeof MonComposant>;

export default meta;

type Story = StoryObj<typeof meta>;

```

### Étape 5 : Préparation du rendu de la story

Préparez le rendu de la story en définissant la structure du composant Storybook.

**Exemple :**

```typescript

// Préparation du rendu de la story

const render = (args: any) => _renderWithWrapper(MonComposant, { ...args }, args.slots ? { ...args.slots } : null);

// Préparation de la template d'une story

const Template: any = (args: any) => ({

    components: { MonComposant },

    render: () => render(args),

    name

});

```

### Étape 6 : Export des stories

Exportez les stories pour différents cas d'utilisation de votre composant.

**Exemple :**

```typescript

// Export des stories

export const Generic: Story = Template.bind({});

Generic.args = {

    // Props

};

export const Variant1: Story = Template.bind({});

Variant1.args = {

    ...Generic.args,

    // Additional props

};

```

## Exemples

### Exemple simple de création d'une story : 

`src/components/FieldLabel/FieldLabel.stories.tsx`

```typescript

// Exemple de fichier de story pour un composant FieldLabel

// src/components/FieldLabel/FieldLabel.stories.tsx

// Import des fonctions helpers

// @ts-ignore

import { _renderWithWrapper, _renderTemplate } from '@/helpers/storybook';

// Import des éléments de Storybook

import type { Meta, StoryObj } from '@storybook/vue3';

import { betaBadges } from '@sb/configs/badges';

// Import du composant

import FieldLabel from './FieldLabel.vue';

// Configuration de la structure de la story

const meta = {

    title: 'Path/To/FieldLabel',

    component: FieldLabel,

    tags: ['autodocs'],

    argTypes: {

        // Props devraient suivre la même structure que les props du composant

    },

    args: {

        // Valeurs par défaut

    },

    parameters: {

        badges: betaBadges,

    },

} satisfies Meta<typeof FieldLabel>;

export default meta;

type Story = StoryObj<typeof meta>;

// Préparation du rendu de la story

const render = (args: any) => _renderWithWrapper(FieldLabel, { ...args }, args.slots ? { ...args.slots } : null);

// Préparation de la template d'une story

const Template: any = (args: any) => ({

    components: { FieldLabel },

    render: () => render(args),

    name

});

// Exportation des stories

export const Generic: Story = Template.bind({});

Generic.args = {

    // Props

};

```

### Exemple pour personnaliser le composant StorybookWrapper : 

`src/components/MultiSelect/MultiSelect.stories.ts`

```typescript

// Exemple pour personnaliser le composant StorybookWrapper

// src/components/MultiSelect/MultiSelect.stories.ts

export const WithFloatingOptions: Story = (args: any) => ({

    render: () => _renderWithWrapper(MultiSelect, { ...args }, args.slots ? { ...args.slots } : null, { height: '160px' }),

});

WithFloatingOptions.args = {

    id: 'multi-select-floating-options',

    items: cloneDeep(items),

    label: 'Multi select with floating options',

    fieldLabel: 'Multi select with floating options',

    withFloatingOptions: true,

};

```

### Exemple pour ajouter des slots : 

`src/components/Interactions/UiButton/UiButton.stories.ts`

```typescript

// Exemple pour ajouter des slots

// src/components/Interactions/UiButton/UiButton.stories.ts

import { h } from 'vue';

export const WithSuffixIcon: any = Template.bind({});

WithSuffixIcon.args = {

    ...Generic.args,

    label: 'A Button with suffix icon',

    slots: {

        'suffix-icon': () =>

            h('span', {

                innerHTML: `<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">

            <path d="M0.765625 7.00003H12.7656H0.765625Z" fill="#fff" />

            <path d="M0.765625 7.00003H12.7656" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" />

            <path d="M6.76562 1V13" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" />

        </svg>`,

            }),

    },

};

```

### Exemple pour créer une story avec un fichier mock : 

`src/components/MultiSelect/MultiSelect.stories.ts`

Fichier mock : `MultiSelect.mock.json`

---


Ces exemples détaillent comment configurer différentes stories pour vos composants FSB. En utilisant ces exemples, vous pouvez personnaliser et configurer les stories de vos composants FSB de manière flexible en fonction de vos besoins.

