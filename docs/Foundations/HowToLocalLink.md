# Étapes pour l'importation de la version locale de front-storybook dans un projet externe (front-XXX)

Pour lier localement la bibliothèque FSB à un projet externe, suivez ces étapes :

## Étape 1 : Installation du projet

Utilisez la commande npm install pour installer la bibliothèque FSB dans votre projet. Depuis le répertoire racine de votre projet externe (par exemple, front-admin), exécutez la commande suivante :

```bash
npm install front-storybook@git+ssh://git@gitlab.com/adcleek/front_storybook.git#develop
```

## Étape 2 : Création du lien local

Depuis le répertoire de votre bibliothèque FSB (par exemple, front_storybook), exécutez les commandes suivantes pour créer un lien local :

```bash
npm link
```

**Chaque fois** que vous avez fini vos modifications et la lib est prête à tester en locale, par exemple mettre ```console.log('coucou')``` quelques parts pour tester, il faut rebuild la lib :

```bash
npm run build-lib
```


## Étape 3 : Configuration du projet externe

Dans le projet externe (front-admin), effectuez les étapes suivantes pour lier la bibliothèque FSB localement :

- Pour la première liaison :
```bash
rm -rf package-lock.json
npm uninstall front-storybook -f
npm link front-storybook -f
```

## Extra (normalement les configs ont déjà été mises en place) :

- Configurations spécifiques dans le projet externe :
Assurez-vous que la configuration de résolution des liens symboliques est correcte. Dans le fichier ```vue.config.js``` du projet externe, ajoutez ou modifiez la configuration suivante :

```javascript
// vue.config.js
const path = require('path');
const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
    transpileDependencies: true,
    chainWebpack: (config) => {
        config.resolve.symlinks(false);
        config.resolve.alias.set('vue', path.resolve(__dirname, './node_modules/@vue/compat'));
});

```

- Assurez-vous que votre fichier ```vite.lib.config.ts``` dans la bibliothèque FSB contient la configuration suivante pour éviter les problèmes de dépendances duplicatas :

```typescript
// vite.lib.config.ts
const libConfigs = defineConfig({
    resolve: {
        dedupe: ['vue', '@vue/compat'],
    },
});
```

En suivant ces étapes, vous devriez pouvoir lier votre bibliothèque FSB localement à votre projet externe sans problème. N'oubliez pas de répéter les étapes 2 chaque fois que vous apportez des modifications à la bibliothèque FSB.

### Sources

- https://github.com/vuejs/core/issues/2064#issuecomment-797365133