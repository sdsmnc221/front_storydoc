

# Création de composants Front Storybook (FSB)

Cette documentation est destinée aux développeurs internes de Front Storybook (FSB) pour les guider dans la création de nouveaux composants en respectant les normes établies. Les composants FSB reposent sur Vue 3 (avec un possible support pour Vue 2) et utilisent les composants de Vue Bootstrap via Vue-compat.

## Prérequis

Avant de commencer à créer un composant FSB, assurez-vous que vous avez les éléments suivants :

- Vue.js 3 (ou Vue 2 si nécessaire)

- Typescript 4.9

- Préprocesseur SASS pour les styles

- Compréhension de la convention BEM (Block, Element, Modifier) pour le nommage CSS

## Processus de création de composants

### Étape 1 : Organisation

Avant de créer un composant, réfléchissez à la manière dont il s'intègre dans la structure de la bibliothèque FSB. Déterminez la catégorie parente (par exemple, Layout, Navigation, Form, etc.) dans laquelle le composant doit être placé. Créez un dossier portant le nom du composant en Pascal Case dans le dossier de la catégorie parente.

### Étape 2 : Fichiers de base

Dans le dossier du composant, créez un fichier Vue en utilisant le nom du composant en Pascal Case comme nom de fichier. Par exemple, si le composant s'appelle "MonComposant", le nom du fichier sera "MonComposant.vue".

### Étape 3 : Structure du composant

Dans le fichier Vue, créez le composant en utilisant le nom de la classe parente dans la balise `<template>`. Préfixez la classe CSS avec "fsb-" suivi du nom du composant en kebab case. Vous pouvez également créer la structure de votre composant Vue ici.

```typescript

<template>

    <div class="fsb-mon-composant">

        <!-- Contenu de votre composant -->

    </div>

</template>

```

### Étape 4 : Export du composant

Ajoutez une balise `<script lang="ts">` pour exporter le composant avec le nom du composant en kebab case comme suit :

```typescript

<script lang="ts">

    export const name = 'mon-composant';

</script>

```

Notez que cette étape ne concerne que l'export du composant pour une utilisation ultérieure. Toute la logique JavaScript doit être définie dans la balise `<script setup lang="ts>`.

### Étape 5 : Logique

Dans cette section, vous pouvez utiliser les syntaxes de Vue 3 conformément au [guide de migration Vue 3](https://v3-migration.vuejs.org/) pour définir la logique de votre composant.

```typescript

<script lang="ts" setup>

    // Votre logique de composant Vue 3 ici

</script>

```

### Étape 6 : Styles (utilisant BEM)

Lorsque vous créez des styles pour votre composant, assurez-vous de suivre la convention BEM (Block, Element, Modifier) pour le nommage CSS. Vous pouvez en apprendre davantage sur BEM en consultant le guide [BEM 101](https://css-tricks.com/bem-101/).

```typescript

<style lang="scss">

    .fsb-mon-composant {

        /* Styles de votre composant en suivant BEM */

    }

</style>

```

### Étape 7 : Exemples

Voici un exemple de structure de fichier pour un composant "FieldLabel" situé dans la catégorie "Form" :

```

/src/components/Form/FieldLabel/FieldLabel.vue

```

```typescript
<template>
    <div class="fsb-field-label">
        <label :for="forField || label">{{ label }}</label>
        <check-field :status="!required" v-if="required" />
    </div>
</template>

<script lang="ts">
    export const name = 'field-label';
</script>

<script lang="ts" setup>
    import CheckField from '../CheckField/CheckField.vue';

    interface Props {
        label: string;
        forField?: string;
        required?: boolean;
    }

    withDefaults(defineProps<Props>(), {
        required: false,
    });
</script>

<style lang="scss">
    .fsb-field-label {
        display: flex;
        align-items: center;

        margin-bottom: 4px;

        label {
            color: var(--grayscale-black);
            font-weight: 700;
            margin: 0;
            padding: 0;
        }
    }
</style>
```

Pour plus d'exemples, consultez les fichiers sources dans la bibliothèque FSB.
