# Build, Export de la bibliothèque FSB et Import de la lib dans un projet Front

## Objectifs de la documentation

Cette documentation a pour objectif de permettre aux développeurs internes de comprendre comment construire la bibliothèque FSB, exporter la bibliothèque avec les composants qu'ils ont créés, et importer ces composants dans d'autres projets Vue.js.

## Structure de la bibliothèque FSB

La bibliothèque FSB suit une structure spécifique avec les répertoires principaux suivants :

- `.storybook/`: Répertoire pour Storybook.

- `src/`: Répertoire principal de la bibliothèque.

-  `src/components/`: Contient les composants Vue.js.

-  `src/styles/`: Contient les styles globaux en Sass.

-  `src/helpers/`: Contient des fichiers d'aide.

-  `src/i18n/`: Gestion des traductions.

-  `src/index.ts`: Point d'entrée pour le build de la bibliothèque.

- `public/`: Ressources publiques.

- `dist/`: Dossier contenant le build de la bibliothèque.

- `storybook-static/`: Dossier contenant le build de Storybook.

- `storybook-static-docs/`: Dossier contenant le build de la documentation de Storybook.

- `vite.config.ts`: Configuration de base pour Vite.

- `vite.lib.config.ts`: Configuration spécifique pour le build de la bibliothèque.

- `vite.storybook.config.ts`: Configuration de Storybook.

- `package.json`: Fichier de configuration de la bibliothèque.

## Étapes pour la construction et l'export

### Étape 1 : Nommer les composants en kebab-case

Au moment de la création des composants Vue, assurez-vous de nommer chaque composant en kebab-case. Par exemple, dans le fichier `MonComposant.vue` :

```typescript

<script lang="ts">

    export const name = 'mon-composant';

</script>

```

### Étape 2 : Exporter les composants

Dans le fichier `src/components/index.ts`, importez le composant que vous avez créé, puis exportez-le :

```typescript

import MonComposant, { name as MonComposantName } from './TypeDeComposants/MonComposant/MonComposant.vue';

export default {

    // autres composants

    [MonComposantName]: MonComposant

};

```

### Étape 3 : Configurer la bibliothèque FSB

La logique de construction de la bibliothèque FSB se trouve dans le fichier `src/index.ts`. Il importe les composants FSB ainsi que les bibliothèques de dépendances comme BootstrapVue et FontAwesome. Il crée ensuite un plugin pour installer ces composants globalement :

```typescript

import components from './components';

import { App } from '@vue/runtime-core';

import { BootstrapVue, TablePlugin, PaginationPlugin, DropdownPlugin, BadgePlugin, LayoutPlugin, BreadcrumbPlugin, SpinnerPlugin } from 'bootstrap-vue';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { library } from '@fortawesome/fontawesome-svg-core';

import { fas } from '@fortawesome/free-solid-svg-icons';

import { far } from '@fortawesome/free-regular-svg-icons';

import './styles/index.scss';

const plugin = {

    install(app: App) {

        console.log('Installing Bootstrap');

        app.use(BootstrapVue).use(DropdownPlugin).use(PaginationPlugin).use(TablePlugin).use(BadgePlugin).use(LayoutPlugin).use(BreadcrumbPlugin).use(SpinnerPlugin);

        console.log('Installing FontAwesome');

        library.add(fas, far);

        app.component('font-awesome-icon', FontAwesomeIcon);

        Object.entries(components).forEach(([name, component]: [name: string, component: any]) => {

            console.log(`Installing component ${name}`);

            app.component(name, component);

        });

    },

};

export default plugin;

```

### Étape 4 : Configurer le build

Les configurations de build se trouvent dans le fichier `vite.lib.config.ts`. Ce fichier indique à Vite comment construire la bibliothèque. Il utilise le plugin `vite-plugin-css-injected-by-js` pour regrouper les styles CSS dans le fichier JavaScript résultant. Le build de la bibliothèque génère des fichiers dans le dossier `dist` :

```typescript

import { defineConfig, mergeConfig } from 'vite';

import path from 'path';

import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';

import baseConfigs from './vite.config';

const libConfigs = defineConfig({

    plugins: [cssInjectedByJsPlugin()],

    build: {

        lib: {

            entry: path.resolve(__dirname, 'src/index.ts'),

            name: 'front-storybook',

            fileName: 'front-storybook',

        },

        rollupOptions: {

            external: ['vue', '@vue/compat'],

            output: {

                globals: {

                    vue: 'Vue',

                },

                exports: 'named',

            },

        },

    },

});

console.log('\n ----------------- CONFIGS: LIB ----------------- ');

console.log(mergeConfig(baseConfigs, libConfigs));

console.log(' ----------------- END ----------------- \n');

export default mergeConfig(baseConfigs, libConfigs);

```

### Étape 5 : Changelog

N'oubliez pas d'ajouter les changements à votre fichier Changelog (`CHANGELOG.md`) chaque fois

 que vous ajoutez un nouveau composant ou effectuez des modifications. Assurez-vous d'incrémenter la version dans votre fichier `package.json` si nécessaire :

```

## Version 0.2.3 2023-10-15

- [ATR] Add some components (admin-nav-bar, expanded-indicator, layout-with-fixed-nav-bar).

```

```javascript
// package.json

  "name": "front_storybook",

  "version": "0.2.3",

```

## Étapes pour l'importation dans un projet externe

Pour importer la bibliothèque FSB dans un projet externe, suivez ces étapes :

### Étape 1 : Installation du projet

Utilisez la commande `npm install` pour installer la bibliothèque FSB dans votre projet. Par exemple, pour le projet `front-admin` :

```bash

npm install front-storybook@git+ssh://git@gitlab.com/adcleek/front_storybook.git#develop

```

### Étape 2 : Importation de la bibliothèque

Dans le fichier `src/main.ts` de votre projet (par exemple, `front-admin`), importez la bibliothèque FSB et créez une instance de votre application. Ensuite, appelez la méthode `install` de la bibliothèque FSB pour installer les composants dans votre application :

```typescript

import fsb from 'front-storybook';

const app = createApp(App);

fsb.install(app);

app.mount('#app');

```

Il est conseillé de installer fsb juste avant de mount app

---

Une fois que vous avez suivi ces étapes, vous pouvez utiliser les composants FSB dans votre projet externe.
